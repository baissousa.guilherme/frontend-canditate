Teste frontend Comunità

# Será avaliado:
- Criatividade
- Técnica (clean code, consumo api, vue js, vuex, performance, deploy)
- Git flow (feature -> merge request)

# Steps:
* 1 download do git (caso não tenha);
* 2 download nodejs (caso não tenha), instale o VUE CLI (p/ criar o project startup);
* 3 Cadastre-se e faça clone do projeto https://gitlab.com/habitatiz.startup/frontend-canditate;
* 4 Crie uma branch com o teu nome a partir da 'master'; 
* 5 Faça o TODO list (logo abaixo);
* 6 Documente as apis utilizadas no postman (https://www.getpostman.com/) e export a collection para a raiz do seu projeto;
* 7 Ao terminar faça commit, e push;
* 8 Crie um 'merge request' da sua branch para 'master'
* 9 Faça o deploy do SPA (heroku, github pages)


# Descrição

Utilizando VUE JS (SPA), VUEX (MVVM), e o serviço https://reqres.in/ (integração de api's), crie uma dashboard com o propósito de cadastro e gestão de usuários.

Sugestão: utilize o vuetify (framework UI para vuejs), tente aplicar o conceito de componentização

API Host: https://reqres.in/api/

# TODO

Crie uma dashboard: 

- Pag. de cadastro -> register (api) -> endpoint: /api/login
- Pag. de login -> (api) -> endpoint: /api/login
- Pag. de lista de users cadastrados (tabela) -> 

	CRUD de usuários
		- listagem
		- edição
		- deletar
		- inserir
		
- Logout -> método

*Consulte a documentação do https://reqres.in/ para identificar os endpoint's.*
*Caso a api esteja indisponível, utilize outro serviço, consulte https://any-api.com/*




